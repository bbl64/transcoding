﻿using System.Collections.Generic;
using TranscodeProcess.InputProcessing.InputStrategy;
using TranscodeProcess.Model;

namespace TranscodeProcess.InputProcessing
{
    /// <summary>
    /// Simple Load Job Service Loader which takes a job load strategy.
    /// </summary>
    public class LoadJobsService
    {
        private readonly IJobInputStrategy _inputStrategy;

        public LoadJobsService(IJobInputStrategy inputStrategy)
        {
            _inputStrategy = inputStrategy;
        }

        public Dictionary<string, Job> LoadJobs()
        {
            return _inputStrategy.GetJobs();
        }
    }
}
