﻿using System.Collections.Generic;
using TranscodeProcess.Model;

namespace TranscodeProcess.InputProcessing.InputStrategy
{
    /// <summary>
    /// Job Input Strategy Interface.
    /// Can be inherited by classes who wants to be an Input to our LoadJobsService
    /// </summary>
    public interface IJobInputStrategy
    {
        Dictionary<string, Job> GetJobs();
    }
}
