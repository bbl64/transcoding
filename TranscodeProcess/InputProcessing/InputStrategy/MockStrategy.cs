﻿using System.Collections.Generic;
using TranscodeProcess.Model;

namespace TranscodeProcess.InputProcessing.InputStrategy
{
    /// <summary>
    /// Strategy for mocking up a few jobs for test
    /// </summary>
    public class MockStrategy : IJobInputStrategy
    {

        public Dictionary<string, Job> GetJobs()
        {
            var jobs = new Dictionary<string, Job>();

            AddJob("900", "Antivirus scan", new List<string> { "100" }, jobs);
            AddJob("800", "Publish ", new List<string> { "700", "601", "608", "603" }, jobs);
            AddJob("700", "Report Statistics", new List<string> { "100" }, jobs);
            AddJob("601", "Transcode 1", new List<string> { "200" }, jobs);
            AddJob("608", "Transcode 2", new List<string> { "200" }, jobs);
            AddJob("603", "Transcode 3", new List<string> { "200" }, jobs);
            AddJob("200", "Rename file", new List<string> { "900" }, jobs);
            AddJob("100", "Upload", new List<string>(), jobs);

            return jobs;
        }

        private void AddJob(string jobId, string jobName, List<string> dependencies, Dictionary<string, Job> jobs)
        {
            var job = new Job( jobId, jobName, dependencies );

            jobs.Add(jobId, job);
        }
    }
}
