﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using TranscodeProcess.Model;

namespace TranscodeProcess.InputProcessing.InputStrategy
{
    /// <summary>
    /// Csv FIle Input Strategy for getting jobs from a CSV File.
    /// </summary>
    public class CsvFileStrategy : IJobInputStrategy
    {
        private readonly string _filePath;

        public CsvFileStrategy(string filePath)
        {
            _filePath = filePath;
        }

        public Dictionary<string, Job> GetJobs()
        {
            Dictionary<string, Job> jobs = new Dictionary<string, Job>();

            var file = File.OpenText(_filePath);
            var csv = new CsvReader(file);
            csv.Configuration.Delimiter = ";";

            ValidateHeaders(csv);

            while (csv.Read())
            {
                var job = ParseJobFields(csv);
                jobs.Add(job.JobId, job);

            }    

            return jobs;
        }

        /// <summary>
        /// Ensure that 3 headers are indeed present in the CSV File
        /// </summary>
        /// <param name="csv">The CSVReader</param>
        private void ValidateHeaders(CsvReader csv)
        {
            csv.ReadHeader();    
            if(csv.FieldHeaders.Length != 3) throw new System.ArgumentException("Provided CSV File must have 3 headers!");
        }


        /// <summary>
        /// Getting fieds but catching error.
        /// YOu could argue also to use the TryGetField but this suffices for this purpose.
        /// </summary>
        /// <param name="csv"></param>
        private Job ParseJobFields(ICsvReaderRow csv)
        {
            try
            {
                var jobIdField = csv.GetField<string>(0);
                var nameField = csv.GetField<string>(1);
                var dependencyField = csv.GetField<string>(2);
                var dependencies = DependencyFromStringToList(dependencyField);

                return new Job(jobIdField, nameField, dependencies);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occured in ParseJobFields when processing CSV file. The error: '{0}'", e);
                throw;
            }

        }

        /// <summary>
        /// Using LINQ to get all dependencies transformed to list. 
        /// On the way making sure that empty values are not considered.
        /// </summary>
        /// <param name="dependencyFields"></param>
        /// <returns></returns>
        private List<string> DependencyFromStringToList(string dependencyFields)
        {
            return
                dependencyFields?
                    .Split(',')
                    .Select(dep => dep.Trim())
                    .Where(dep => !string.IsNullOrEmpty(dep))
                    .ToList()
                ?? new List<string>();
        }
    }
}
