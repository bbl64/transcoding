﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TranscodeProcess.Model
{
    /// <summary>
    /// A Transcoding job which can be a part of a transcode process.
    /// </summary>
    public class Job
    {
        public Job(string jobId, string jobName, List<string> jobDependencies)
        {
            JobId = jobId;
            JobName = jobName;
            JobDependencies = jobDependencies;
        }

        public string JobId { get; set; }

        public string JobName { get; set; }

        public List<string> JobDependencies { get; set; }
    }
}
