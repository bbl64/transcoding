﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TranscodeProcess.Model
{
    /// <summary>
    /// A job group is a list of all the possible jobs that have the dependencies fullfilled and can be started. 
    /// </summary>
    public class JobGroup
    {
        public JobGroup(List<Job> jobs) => GroupJobs = jobs;

        public List<Job> GroupJobs { get; set; }
    }
}
