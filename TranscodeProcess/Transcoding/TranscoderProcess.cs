﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using TranscodeProcess.Model;

namespace TranscodeProcess.Transcoding
{
    /// <summary>
    ///  A transcode process contains one or more jobs(for example “rename file”, “convert from JPG to PNG”), 
    /// equal in execution time, that have to be executed in a particular order.
    /// </summary>
    public class TranscoderProcess
    {

        private readonly Dictionary<string, Job> _allJobs;
        private SortedDictionary<int, JobGroup> _jobGroups;
        private Dictionary<string, int> _jumpCounter;

        public TranscoderProcess(Dictionary<string, Job> jobs )
        {
            _allJobs = jobs;
            _jobGroups = new SortedDictionary<int, JobGroup>();
            _jumpCounter = new Dictionary<string, int>();
        }

        /// <summary>
        /// This method will analyse all jobs.
        /// For this to work one job must come first and have no dependencies. This is the starting job.
        /// This method simply figures out how many "jumps" the current job have from the starting job.
        /// </summary>
        public void JobGroupAnalysis()
        {

            foreach (string key in _allJobs.Keys)
            {
                var job = _allJobs[key];

                var dependencies = job.JobDependencies;

                var maxJumps = 1;
                foreach (string dependency in dependencies)
                {

                    maxJumps = ReturnNewMax(dependency, maxJumps);
                }

                _jumpCounter[job.JobId] = maxJumps;
                AddJobToGroup(maxJumps, job);
            }
        }

        public void AlternativeAnalysis()
        {
            var jobs = _allJobs.Values.ToList();
            var initialJobIndex = 0;
            var initialDependencyIndex = 0;
            var initialMaxValue = 1;

            AnalyseJumpsToStart(jobs, initialJobIndex, initialDependencyIndex, initialMaxValue);
        }


        /// <summary>
        /// Recursive method to calculate the number of "jumps" to the starting job.
        /// The starting job is defined as the one without any dependencies.
        /// </summary>
        /// <param name="dependency"></param>
        /// <param name="groupNumber"></param>
        private IEnumerable<int> JobGroupDivide(string dependency, int groupNumber)
        {
            groupNumber++;

            // Have to check if the dependecy is actually in the map.
            // In theory, you could in future cases have a depedency which is not yet in the list of jobs.
            if (_allJobs.ContainsKey(dependency))
            {
                var dependencyJob = _allJobs[dependency];
                var dependencies = dependencyJob.JobDependencies;

                if (dependencies.Count == 0)
                {
                    yield return groupNumber;
                }
                else
                {
                    foreach (string jobToStart in dependencies)
                    {
                        yield return JobGroupDivide(jobToStart, groupNumber).Max();
                    }
                }
            }
            yield return groupNumber;
        }

        /// <summary>
        /// Alternative Method which is only recursive but is taken the same time.
        /// Could maybe have done some divide a conqueur.
        /// I really do prefer the first approach since it simplifies it.
        /// </summary>
        /// <param name="allJobs"></param>
        /// <param name="index"></param>
        /// <param name="dependencyIndex"></param>
        /// <param name="previosMaxVal"></param>
        private void AnalyseJumpsToStart( List<Job> allJobs, int index, int dependencyIndex, int previosMaxVal  )
        {
            // At the end of the list
            if (index == allJobs.Count) return;

            // Get the current job based on index
            var currentJob = allJobs[index];
            var dependencies = currentJob.JobDependencies;

            // Check if all dependencies have been finished for this index
            var newMax = 1;
            if (dependencies.Count > dependencyIndex)
            {
                var dependency = dependencies[dependencyIndex];
                newMax = ReturnNewMax(dependency, previosMaxVal);

                dependencyIndex++;
            }
            else
            {
                // The index is now finished so save the max val
                _jumpCounter[currentJob.JobId] = previosMaxVal;
                AddJobToGroup(previosMaxVal, currentJob);
                index++;
                dependencyIndex = 0;
            }
                
            // Jump again
            AnalyseJumpsToStart(allJobs, index, dependencyIndex, newMax);
        }

        /// <summary>
        /// Simple method that looks for number of jumps that have already been calculated to save recursive calls.
        /// </summary>
        /// <param name="dependency"></param>
        /// <param name="previosMaxVal"></param>
        /// <returns></returns>
        private int ReturnNewMax( string dependency, int previosMaxVal )
        {
           var jumps = !_jumpCounter.ContainsKey(dependency) ? JobGroupDivide(dependency, 1).Max() : _jumpCounter[dependency] + previosMaxVal;

           return jumps > previosMaxVal ? jumps : previosMaxVal;
        }


        /// <summary>
        /// Responsible for adding the job to the right group based on the number of jumps.
        /// The number of jumps indicate in which order the group is processed (starting from 0).
        /// </summary>
        /// <param name="maxJumps"></param>
        /// <param name="job"></param>
        private void AddJobToGroup(int maxJumps, Job job)
        {
            var jobGroup = !_jobGroups.ContainsKey(maxJumps) ? new JobGroup(new List<Job>()) : _jobGroups[maxJumps];

            jobGroup.GroupJobs.Add(job);
            _jobGroups[maxJumps] = jobGroup;
        }

        /// <summary>
        /// Simple method for print result
        /// </summary>
        public void PrintGroups()
        {
            foreach (int key in _jobGroups.Keys)
            {
                Console.WriteLine("Group Number: " + key);

                var jobGroup = _jobGroups[key];
                foreach (Job job in jobGroup.GroupJobs)
                {
                    Console.WriteLine("Job id: " + job.JobId + " - Name: " + job.JobName);
                }
                Console.WriteLine("");
            }
        }

    }
}
