﻿using System;
using TranscodeProcess.InputProcessing;
using TranscodeProcess.InputProcessing.InputStrategy;
using TranscodeProcess.Transcoding;

namespace TranscodeProcess
{
    class Program
    {
        static void Main(string[] args)
        {
            // Get jobs
            var filePath = "InputProcessing/jobs.csv";

            // First approach
            ApproachOne(filePath);

            Console.WriteLine("\n");

            // Another approach
            ApproachTwo(filePath);

        }

        /// <summary>
        /// Loops over all jobs and using recursive method to find jumps
        /// </summary>
        /// <param name="filePath"></param>
        private static void ApproachOne(string filePath)
        {
            Console.WriteLine("Approach 1 \n");
            var loadService = new LoadJobsService(new CsvFileStrategy(filePath));
            //LoadJobsService loadService = new LoadJobsService(new MockStrategy());
            var jobs = loadService.LoadJobs();

            // Make the analysis
            var transcodeProcess = new TranscoderProcess(jobs);
            transcodeProcess.JobGroupAnalysis();

            // Print Reslt
            transcodeProcess.PrintGroups();
        }

        /// <summary>
        /// Recursive loop of all jobs but still using same method as approach 1 to get jumps
        /// </summary>
        /// <param name="filePath"></param>
        private static void ApproachTwo(string filePath)
        {
            Console.WriteLine("Approach 2 \n");
            var loadService = new LoadJobsService(new CsvFileStrategy(filePath));
            //LoadJobsService loadService = new LoadJobsService(new MockStrategy());
            var jobs = loadService.LoadJobs();

            // Make the analysis
            var transcodeProcess = new TranscoderProcess(jobs);
            transcodeProcess.AlternativeAnalysis();

            // Print Reslt
            transcodeProcess.PrintGroups();
        }

    }

}
